module Benches
  module Matchers

    RSpec::Matchers.define :run do |repetitions|

      chain :time_in_less_than do |duration|
        @duration = duration
      end

      chain :times_in_less_than do |duration|
        @duration = duration
      end

      match do |actual|
        Benchmark.measure do
          repetitions.times do
            actual.call
          end
        end.utime.seconds < @duration
      end

      failure_message_for_should do |actual|
        "expected code to run in less than #{@duration.inspect}"
      end

      failure_message_for_should_not do |actual|
        "expected code not to run in less than #{@duration.inspect} seconds"
      end

      description do
        "run in less than #{@duration.inspect}"
      end
    end

  end
end

